package com.example.gamblflappyaviatorv2.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

@SuppressLint("ViewConstructor")
class FlappyAviatorGameView(context: Context, private val gameAssets: GameAssets): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f

    private var dY = 0.0f
    private var impulse = 0.0f
    private var pillarWidth = 100.0f
    private var pillarHoleRadius = 50.0f
    private var pillarMinDistance = 50.0f
    private var pillarSpeed = 2.0f

    private var pillars = ArrayList<Pillar>()

    private var record = 0

    private lateinit var bPillar: Bitmap

    private var state = PAUSE

    private var gameInterface: FlappyPlaneInterface? = null

    companion object {
        const val PAUSE = 0
        const val FLYING = 1
    }

    init {
        gameAssets.pillar?.let {
            val rotationMatrix = Matrix()
            rotationMatrix.postRotate(180.0f)
            bPillar = Bitmap.createBitmap(it, 0,0,it.width, it.height, rotationMatrix, true )
        }
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 2.0f
            planeX = mWidth / 5.0f
            impulse = mHeight / 600.0f * 3.0f
            pillarWidth = mWidth / 20.0f
            pillarHoleRadius = (gameAssets.plane?.height ?:100) / 2.0f * 2.0f
            pillarMinDistance = (gameAssets.plane?.width ?: 75) * 1.5f + pillarWidth * 1.5f
            pillarSpeed = mWidth / 600.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) {
                updatePillars()
                updatePlane()
                drawPillars(it)
            }
            drawPlane(it)
            drawBounds(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> return true
            MotionEvent.ACTION_UP -> {
                if(state == FLYING) tap()
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: FlappyPlaneInterface) {
        gameInterface = i
    }

    fun restart() {
        pillars.clear()
        state = FLYING
        planeY = mHeight / 2.0f
        record = 0
        gameInterface?.updatePoints(record)
        pillarSpeed = mWidth / 600.0f
        tap()
    }

    fun resetGame() {
        pillars.clear()
        state = PAUSE
        planeY = mHeight / 2.0f
        record = 0
        gameInterface?.updatePoints(record)
        pillarSpeed = mWidth / 600.0f
        planeAngle = 0.0f
        tap()
    }

    //// Private

    private fun drawPlane(c: Canvas) {
        gameAssets.plane?.let {
            val rotationMatrix = Matrix()
            rotationMatrix.preScale(0.5f, 0.5f)
            rotationMatrix.postRotate(planeAngle)
            val rotatedPlane = Bitmap.createBitmap(it, 0,0,it.width, it.height , rotationMatrix, true )
            val p = Paint()
            p.color = Color.WHITE
            val offY = if(planeAngle >= 0) -it.height / 2.0f else -rotatedPlane.height.toFloat()
            c.drawBitmap(rotatedPlane, planeX, planeY + offY, p)
        }
    }

    private fun drawBounds(c : Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.style = Paint.Style.FILL
        c.drawRect(0.0f, 0.0f, mWidth.toFloat(), 10.0f, p)
        c.drawRect(0.0f, mHeight - 10.0f, mWidth.toFloat(), mHeight.toFloat(), p)
    }

    private fun drawPillars(c : Canvas) {
        val p = Paint()
        for(pillar in pillars) {
            gameAssets.pillar?.let { c.drawBitmap(it,null, Rect((pillar.x - pillarWidth / 2).toInt(),10,(pillar.x + pillarWidth / 2).toInt(), (pillar.holeY - pillarHoleRadius).toInt()), p) }
            c.drawBitmap(bPillar,null, Rect((pillar.x - pillarWidth / 2).toInt(), (pillar.holeY + pillarHoleRadius).toInt(),(pillar.x + pillarWidth / 2).toInt(), mHeight - 10), p)
        }
    }

    private fun updatePlane() {
        planeY += dY

        //dY += impulse / 20.0f
        dY += impulse / 35.0f
        planeAngle = ((dY / (impulse / 30.0f))).coerceAtLeast(-30.0f).coerceAtMost(30.0f)

        val noseY = planeY - ((gameAssets.plane?.width ?:75) / 2.0f) * sin(Math.toRadians((-planeAngle).toDouble())) //- if(planeAngle > 0) plane.height / 2.0f else 0.0f
        val noseX = planeX + ((gameAssets.plane?.width ?:75) / 2.0f) * cos(Math.toRadians((-planeAngle).toDouble()))
        if(noseY < 0.0f || noseY >= mHeight || planeY >= mHeight) {
            state = PAUSE
            gameInterface?.onGameEnd()
        }

        for(pillar in pillars) if(noseX in (pillar.x - pillarWidth / 2)..(pillar.x + pillarWidth / 2)
            || planeX in (pillar.x - pillarWidth / 2)..(pillar.x + pillarWidth / 2)
            || (planeX < (pillar.x - pillarWidth / 2) && noseX > (pillar.x + pillarWidth / 2))) {
            if(intercepts(planeX, planeY - (gameAssets.plane?.height ?:100) / 2.0f, noseX.toFloat(), noseY.toFloat() - (gameAssets.plane?.height ?:100) / 2.0f,pillar.x - pillarWidth / 2.0f, 0.0f, pillar.x - pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius)
                || intercepts(planeX, planeY, noseX.toFloat(), noseY.toFloat(),pillar.x - pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius, pillar.x - pillarWidth / 2.0f, mHeight.toFloat())) {
                state = PAUSE
                gameInterface?.onGameEnd()
            }

            if(intercepts(planeX, planeY - (gameAssets.plane?.height ?:100) / 4.0f, noseX.toFloat(), noseY.toFloat(), pillar.x - pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius, pillar.x + pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius)
                || intercepts(planeX, planeY - (gameAssets.plane?.height ?:100) / 2.0f, noseX.toFloat(), noseY.toFloat() - (gameAssets.plane?.height ?:100) / 2.0f, pillar.x - pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius, pillar.x + pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius)) {
                state = PAUSE
                gameInterface?.onGameEnd()
            }
        }
    }

    private fun updatePillars() {
        if(pillars.isEmpty()) createPillar()
        else if(pillars.last().x + pillarMinDistance < mWidth) {
            if(Random.nextInt(100) < (mWidth - (pillars.last().x + pillarMinDistance)) / (mWidth / 600.0f)) createPillar()
        }
        var n = 0
        while(n < pillars.size) {
            pillars[n].x -= pillarSpeed
            if(pillars[n].x + pillarWidth / 2.0f < planeX && !pillars[n].passed) {
                record++
                gameInterface?.updatePoints(record)
                pillars[n].passed = true
            }
            if(pillars[n].x < - pillarWidth / 2) {
                pillars.removeAt(n)
                n--
            }
            n++
        }
        pillarSpeed += 0.01f
    }

    private fun createPillar() {
        pillars.add(Pillar(mWidth.toFloat() + pillarWidth / 2, Random.nextInt(10 + pillarHoleRadius.toInt(), mHeight - 10 - pillarHoleRadius.toInt()), false))
    }

    private fun tap() {
        dY = -impulse
    }

    private fun intercepts(ax1: Float, ay1: Float, ax2: Float, ay2: Float, bx1: Float, by1: Float, bx2: Float, by2: Float): Boolean {
        val v1 = ((ax2 - ax1) * (ay2 - by1)) - ((ay2 - ay1) * (ax2 - bx1))
        val v2 = ((ax2 - ax1) * (ay2 - by2)) - ((ay2 - ay1) * (ax2 - bx2))
        val v3 = ((bx2 - bx1) * (by2 - ay1)) - ((by2 - by1) * (bx2 - ax1))
        val v4 = ((bx2 - bx1) * (by2 - ay2)) - ((by2 - by1) * (bx2 - ax2))

        return v1 * v2 < 0 && v3 * v4 < 0
    }

    interface FlappyPlaneInterface {
        fun onGameEnd()
        fun updatePoints(points: Int)
    }
}