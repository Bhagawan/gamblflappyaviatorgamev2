package com.example.gamblflappyaviatorv2.game

data class Pillar(var x : Float, val holeY : Int, var passed : Boolean)
