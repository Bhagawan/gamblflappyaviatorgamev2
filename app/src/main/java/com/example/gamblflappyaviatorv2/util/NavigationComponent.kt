package com.example.gamblflappyaviatorv2.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.gamblflappyaviatorv2.MainViewModel
import com.example.gamblflappyaviatorv2.screens.ErrorScreen
import com.example.gamblflappyaviatorv2.screens.Screens
import com.example.gamblflappyaviatorv2.screens.SplashScreen
import com.example.gamblflappyaviatorv2.screens.WebViewScreen
import com.example.gamblflappyaviatorv2.screens.game.GameScreen
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, viewModel: MainViewModel = viewModel()) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_LOADING_SCREEN.label
    ) {
        composable(Screens.SPLASH_LOADING_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.SPLASH_SCREEN.label) {
            WebViewScreen(webView, viewModel.url)
        }
        composable(Screens.GAME.label) {
            GameScreen(remember { viewModel.gameAssets })
        }
        composable(Screens.ERROR_LOADING_ASSETS.label) {
            ErrorScreen(onClick = viewModel::retryDownload)
        }
    }
    LaunchedEffect("nav") {
        viewModel.navigationFlow.onEach {
            if(it == Screens.SPLASH_SCREEN || it == Screens.GAME) navController.backQueue.clear()
            navController.navigate(it.label)
        }.launchIn(this)
    }
}