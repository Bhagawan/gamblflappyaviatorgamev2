package com.example.gamblflappyaviatorv2.util

import androidx.annotation.Keep

@Keep
data class GamblFlappyAviatorSplashResponse(val url : String)