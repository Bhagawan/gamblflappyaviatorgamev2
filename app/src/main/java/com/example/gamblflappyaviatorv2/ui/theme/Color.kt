package com.example.gamblflappyaviatorv2.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Grey = Color(0xFF3D3D3D)
val Grey_light = Color(0xFF777777)
val Red = Color(0xFF8F3932)
val Green = Color(0xFF4D704E)
val Yellow = Color(0xFF998750)