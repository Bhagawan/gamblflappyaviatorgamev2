package com.example.gamblflappyaviatorv2.screens.game

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.gamblflappyaviatorv2.R
import com.example.gamblflappyaviatorv2.ui.theme.Grey
import com.example.gamblflappyaviatorv2.ui.theme.Red

@Composable
fun BrokePopup() {
    Column(modifier = Modifier
        .wrapContentSize()
        .padding(Dp(10.0f)).border(Dp(2.0f), color = Red )
        .background(color = Grey, shape = RoundedCornerShape(Dp(10.0f)))) {
        Text(text = stringResource(id = R.string.lost), fontSize = TextUnit(30.0f, TextUnitType.Sp), color = Red, modifier = Modifier
            .align(CenterHorizontally))
        Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(id =R.string.desc_reset), modifier = Modifier
            .size(Dp(200.0f)))
    }
}