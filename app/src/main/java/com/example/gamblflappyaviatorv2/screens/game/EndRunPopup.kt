package com.example.gamblflappyaviatorv2.screens.game

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.TabRowDefaults.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.gamblflappyaviatorv2.R
import com.example.gamblflappyaviatorv2.ui.theme.Grey
import com.example.gamblflappyaviatorv2.ui.theme.Yellow

@Composable
fun EndRunPopup(gates: Int, bet: Int, coefficient: Float) {
    Column(modifier = Modifier
        .wrapContentSize()
        .padding(Dp(10.0f))
        .border(Dp(3.0f), color = Yellow, shape = RoundedCornerShape(Dp(20.0f)))
        .background(color = Grey, shape = RoundedCornerShape(Dp(20.0f)))) {
        Text(text = stringResource(id = R.string.flight_end), fontSize = TextUnit(30.0f, TextUnitType.Sp), color = Yellow, modifier = Modifier
            .align(Alignment.CenterHorizontally))
        Box(modifier = Modifier
            .height(Dp(22.0f))
            .fillMaxWidth()
            .padding(Dp(10.0f))) {
            Divider(color = Yellow, thickness = Dp(3.0f))
        }
        Row(modifier = Modifier.fillMaxWidth().padding(vertical = Dp(3.0f))) {
            Text(text = stringResource(id = R.string.points), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
            Text(text = gates.toString(), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
        }
        Row(modifier = Modifier.fillMaxWidth().padding(vertical = Dp(3.0f))) {
            Text(text = stringResource(id = R.string.bet), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
            Text(text = bet.toString(), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
        }
        Row(modifier = Modifier.fillMaxWidth().padding(vertical = Dp(3.0f))) {
            Text(text = stringResource(id = R.string.coeff), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
            Text(text = "${coefficient}x", fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
        }
        Row(modifier = Modifier.fillMaxWidth().padding(top = Dp(3.0f), bottom = Dp(13.0f))) {
            Text(text = stringResource(id = R.string.win), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
            Text(text = (bet * coefficient).toInt().toString(), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f)
                    .padding(horizontal = Dp(5.0f)))
        }
    }
}
