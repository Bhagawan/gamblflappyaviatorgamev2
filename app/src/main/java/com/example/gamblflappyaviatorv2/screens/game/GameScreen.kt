package com.example.gamblflappyaviatorv2.screens.game

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.gamblflappyaviatorv2.R
import com.example.gamblflappyaviatorv2.game.FlappyAviatorGameView
import com.example.gamblflappyaviatorv2.game.GameAssets
import com.example.gamblflappyaviatorv2.ui.theme.*
import com.example.gamblflappyaviatorv2.util.Prefs
import com.example.gamblflappyaviatorv2.util.UrlAsset_background
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

@Composable
fun GameScreen(gameAssets: GameAssets = GameAssets(null, null)) {
    val viewModel : GameViewModel = viewModel<GameViewModel>().apply {
        if(updateWallet) updateWallet(Prefs.getWallet(LocalContext.current))
    }
    var gameBackground by remember { mutableStateOf<ImageBitmap?>(null) }
    DisposableEffect("logo") {
        val picasso = Picasso.get()

        val target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                gameBackground = bitmap?.asImageBitmap()
            }
        }

        picasso.load(UrlAsset_background).into(target)

        onDispose {
            picasso.cancelRequest(target)
        }
    }

    val restartState = viewModel.gameRestart.collectAsState(initial = false)
    val resetState = viewModel.gameReset.collectAsState(initial = false)

    val wallet = viewModel.wallet.collectAsState()
    if(Prefs.getWallet(LocalContext.current) != wallet.value) Prefs.updateWallet(LocalContext.current, wallet.value)

    Column(modifier = Modifier
        .fillMaxSize()
        .background(color = Grey)) {
        Box(modifier = Modifier
            .fillMaxWidth()
            .height(Dp(30.0f))) {
            Text(text = wallet.value.toString(), textAlign = TextAlign.Center, fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White,
                modifier = Modifier.fillMaxWidth())
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(Dp(5.0f)), contentAlignment = Alignment.CenterEnd) {
                Button(onClick = { viewModel.resetGame() },
                    modifier = Modifier.size(Dp(30.0f)),
                    shape = MaterialTheme.shapes.medium,
                    colors = ButtonDefaults.buttonColors(contentColor = Red, backgroundColor = Grey_light)) { }
                Image(
                    modifier = Modifier.size(Dp(30.0f)),
                    painter = painterResource(id = R.drawable.ic_baseline_reload),
                    contentDescription = stringResource(id = R.string.desc_reset)
                )
            }
        }

        Box(modifier = Modifier
            .weight(7.0f, true)
            .fillMaxSize()
            .background(Grey)) {
            gameBackground?.let { Image(it, "back",modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillHeight) }

            AndroidView(factory = { FlappyAviatorGameView(it, gameAssets) },
                modifier = Modifier.fillMaxSize(),
                update = {
                    if(restartState.value && !resetState.value) {
                        it.restart()
                        viewModel.clearCommands()
                    }
                    if(resetState.value) {
                        it.resetGame()
                        viewModel.clearCommands()
                    }

                    it.setInterface(object : FlappyAviatorGameView.FlappyPlaneInterface {
                        override fun onGameEnd() {
                            viewModel.endRun()
                        }

                        override fun updatePoints(points: Int) {
                            viewModel.updateScore(points)
                        }

                    })
                })

        }
        Row(modifier = Modifier
            .weight(3.0f, true)
            .padding(Dp(10.0f)), verticalAlignment = Alignment.CenterVertically) {
            Column(modifier = Modifier
                .weight(3.0f, true)
                .padding(end = Dp(10.0f))) {
                Box(modifier = Modifier
                    .weight(1.0f, true)
                    .align(Alignment.CenterHorizontally)) {
                    Text(stringResource(id = R.string.header_bet), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp),
                        modifier = Modifier
                            .wrapContentSize()
                            .align(Alignment.Center))
                }

                Box(modifier = Modifier
                    .weight(1.0f, true)
                    .fillMaxSize()
                    .background(color = Grey_light, shape = RoundedCornerShape(Dp(30.0f)))
                    .padding(Dp(10.0f))
                    .align(Alignment.CenterHorizontally)) {
                    Text(text = viewModel.bet.collectAsState().value.toString(), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp), textAlign = TextAlign.Center,
                        modifier = Modifier
                            .wrapContentSize()
                            .align(Alignment.Center))
                }

                Row(modifier = Modifier
                    .fillMaxSize()
                    .weight(1.0f, true)) {
                    Button(onClick = { viewModel.increaseBet(1) }, contentPadding = PaddingValues(Dp(1.0f)),
                        modifier = Modifier
                            .weight(1.0f)
                            .padding(start = Dp(5.0f), end = Dp(5.0f)),
                        shape = RoundedCornerShape(Dp(20.0f)),
                        colors = ButtonDefaults.buttonColors(contentColor = Color.White, backgroundColor = Yellow)) {
                        Text(stringResource(id = R.string.increase_1), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                    }
                    Button(onClick = { viewModel.increaseBet(10) }, contentPadding = PaddingValues(Dp(1.0f)),
                        modifier = Modifier
                            .weight(1.0f, true)
                            .padding(start = Dp(5.0f), end = Dp(5.0f)),
                        shape = RoundedCornerShape(Dp(20.0f)),
                        colors = ButtonDefaults.buttonColors(contentColor = Color.White, backgroundColor = Yellow)) {
                        Text(stringResource(id = R.string.increase_10), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                    }
                    Button(onClick = { viewModel.increaseBet(100) }, contentPadding = PaddingValues(Dp(1.0f)),
                        modifier = Modifier
                            .weight(1.0f, true)
                            .padding(start = Dp(5.0f), end = Dp(5.0f)),
                        shape = RoundedCornerShape(Dp(20.0f)),
                        colors = ButtonDefaults.buttonColors(contentColor = Color.White, backgroundColor = Yellow)) {
                        Text(stringResource(id = R.string.increase_100), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                    }
                }
            }
            Box(modifier = Modifier
                .weight(1.0f)
                .fillMaxSize()) {
                Button(onClick = { viewModel.startRun() }, contentPadding = PaddingValues(Dp(1.0f)),
                    modifier = Modifier
                        .fillMaxSize()
                        .aspectRatio(1.0f),
                    shape = MaterialTheme.shapes.medium,
                    colors = ButtonDefaults.buttonColors(contentColor = Color.White, backgroundColor = Green)) { }
                Text(stringResource(id = R.string.btn_play), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp),
                    modifier = Modifier.align(Alignment.Center))
            }
        }
    }

    val endPopup = viewModel.gameEnd.collectAsState()
    val brokePopup = viewModel.gameBroke.collectAsState()

    if(endPopup.value && !brokePopup.value) {
        Box(modifier = Modifier.fillMaxSize().clickable { viewModel.hideEndPopup() }, contentAlignment = Alignment.Center) {
            EndRunPopup(gates = viewModel.currentGates, bet = viewModel.bet.collectAsState().value, coefficient = viewModel.coeff.collectAsState().value)
        }
    }
    if(brokePopup.value) {
        Box(modifier = Modifier.fillMaxSize().clickable { viewModel.resetGame() }, contentAlignment = Alignment.Center) {
            BrokePopup()
        }
    }
}