package com.example.gamblflappyaviatorv2.screens

enum class Screens(val label: String) {
    SPLASH_LOADING_SCREEN("splash_loading"),
    SPLASH_SCREEN("splash"),
    GAME("game"),
    ERROR_LOADING_ASSETS("error")
}