package com.example.gamblflappyaviatorv2

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gamblflappyaviatorv2.game.GameAssets
import com.example.gamblflappyaviatorv2.screens.Screens
import com.example.gamblflappyaviatorv2.util.GamblFlappyAviatorServerClient
import com.example.gamblflappyaviatorv2.util.UrlAsset_pillar
import com.example.gamblflappyaviatorv2.util.UrlAsset_plane
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel: ViewModel() {
    private var request: Job? = null

    private val _navigationFlow = MutableSharedFlow<Screens>( 1)
    val navigationFlow = _navigationFlow.asSharedFlow()

    var url = ""

    val gameAssets = GameAssets(null, null)

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = GamblFlappyAviatorServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToGame()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToGame()
                            }
                            else -> viewModelScope.launch {
                                url ="https://${splash.body()!!.url}"
                                _navigationFlow.tryEmit(Screens.SPLASH_SCREEN)
                            }
                        }
                    } else switchToGame()
                } else switchToGame()
            }
        } catch (e: Exception) {
            switchToGame()
        }
    }

    fun retryDownload() {
        _navigationFlow.tryEmit(Screens.SPLASH_LOADING_SCREEN)
        switchToGame()
    }

    private var planeDownload: Job? = null
    private var pillarDownload: Job? = null

    private fun switchToGame() {
        if(planeDownload?.isActive == true) {
            planeDownload?.cancel()
        }
        planeDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            val target = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    errorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        gameAssets.plane = bitmap
                        if(gameAssets.plane != null && gameAssets.pillar != null) gameScreen()
                    } else {
                        errorScreen()
                    }
                }
            }

            picasso.load(UrlAsset_plane).into(target)

        }
        if(pillarDownload?.isActive == true) pillarDownload?.cancel()
        pillarDownload = viewModelScope.launch {
            val picasso = Picasso.get()
            val target = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    errorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        gameAssets.pillar = bitmap
                        if(gameAssets.plane != null && gameAssets.pillar != null) gameScreen()
                    } else {
                        errorScreen()
                    }
                }
            }
            picasso.load(UrlAsset_pillar).into(target)
        }
    }

    private fun gameScreen() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.GAME)
        }
    }

    private fun errorScreen() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.ERROR_LOADING_ASSETS)
        }
    }

}